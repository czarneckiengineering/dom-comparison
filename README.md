# DOM XML Document Comparison #

## summary ##

This repo holds the code for a simple utility class to compare two DOM objects with _extremely_ flexible structure

The two DOM objects are read into two maps from XPath to node value; then the two maps are iterated over.