package com.thenational.murex.officialrates.util;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class DOMUtil {
	private static String indent = "";

	public static Document createDocument() {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.newDocument();
			return doc;
		} catch (ParserConfigurationException e) {
		}
		return null;
	}

	public static Document loadDocument(String filename) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			Document doc = factory.newDocumentBuilder().parse(
					new File(filename));
			return doc;
		} catch (SAXException e) {
			System.out.println(e);
		} catch (ParserConfigurationException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return null;
	}

	public static class NodeComparison {

		private Document doc;

		private Node leftDifference;

		private Node rightDifference;

		private boolean isFinished = false;

		public NodeComparison() {
			this.doc = createDocument();
		}

		public NodeComparison(Document doc) {
			this.doc = doc;
		}

		public void compare(Node leftNode, Node rightNode) {
			System.out.println(indent + "compare(" + leftNode	+ ", " + rightNode + ")");
			indent = indent + "   ";

			compareNullNodes(leftNode, rightNode);

			// compare types
			if (!isFinished)
				compareTypes(leftNode, rightNode);

			// compare names
			if (!isFinished)
				compareNames(leftNode, rightNode);

			// compare content
			if (!isFinished)
				compareSimpleContent(leftNode, rightNode);

			if (!isFinished)
				compareContent(leftNode, rightNode);

			indent = indent.substring(3);

		}

		public void compareNullNodes(Node leftNode, Node rightNode) {
			if (leftNode == null && rightNode != null) {
				rightDifference = doc.importNode(rightNode, true);
			}

			if (rightNode == null && leftNode != null) {
				leftDifference = doc.importNode(leftNode, true);
			}

			if (leftNode == null || rightNode == null) {
				isFinished = true;
			}
		}

		public void compareTypes(Node leftNode, Node rightNode) {
			if (leftNode.getNodeType() != rightNode.getNodeType()) {
				leftDifference = doc.importNode(leftNode, true);
				rightDifference = doc.importNode(rightNode, true);
				isFinished = true;
			}
		}

		public void compareNames(Node leftNode, Node rightNode) {
			if (!leftNode.getNodeName().equals(rightNode.getNodeName())) {
				leftDifference = doc.importNode(leftNode, true);
				rightDifference = doc.importNode(rightNode, true);
				isFinished = true;
			}
		}

		public void compareSimpleContent(Node leftNode, Node rightNode) {
			if (leftNode.getChildNodes().getLength() == 1
					&& rightNode.getChildNodes().getLength() == 1 && leftNode.getNodeType() == Node.ELEMENT_NODE) {
				if (!leftNode.getTextContent().equals(
						rightNode.getTextContent())) {
					leftDifference = doc.importNode(leftNode, true);
					rightDifference = doc.importNode(rightNode, true);
					isFinished = true;
				}
			}
		}

		public void compareContent(Node leftNode, Node rightNode) {
			switch (leftNode.getNodeType()) {
			case Node.DOCUMENT_NODE:
				NodeComparison comparison = new NodeComparison(doc);
				comparison.compare(leftNode.getFirstChild(), rightNode.getFirstChild());
				leftDifference = comparison.getLeftDifference();
				rightDifference = comparison.getRightDifference();
				isFinished = true;
				break;

			case Node.ELEMENT_NODE:
				compareComplexContent(leftNode, rightNode);

			case Node.ATTRIBUTE_NODE:
			case Node.TEXT_NODE:
				isFinished = true;
				break;
			}
		}

		private void compareComplexContent(Node leftNode, Node rightNode) {
			NodeList leftChildren = leftNode.getChildNodes();
			NodeList rightChildren = rightNode.getChildNodes();
			int leftLength = leftChildren.getLength();
			int rightLength = rightChildren.getLength();

			nextLeftUnmatched: for (int leftFirstUnmatched = 0, rightFirstUnmatched = 0; leftFirstUnmatched < leftLength; leftFirstUnmatched++) {
				NodeComparison leftFirstComparison = null, comparison = null;

				if (leftChildren.item(leftFirstUnmatched).getNodeType() == Node.TEXT_NODE)
					continue;

				for (int rightIndex = rightFirstUnmatched; rightIndex < rightLength; rightIndex++) {
					if (rightChildren.item(rightIndex).getNodeType() != Node.TEXT_NODE) {
						comparison = new NodeComparison(doc);

						System.out.println(indent + "compare ["	+ leftFirstUnmatched + ", "	+ rightIndex + "] - " + writeXPath(leftChildren.item(leftFirstUnmatched)) + " - " + writeXPath(rightChildren.item(rightIndex)));
						comparison.compare(leftChildren.item(leftFirstUnmatched), rightChildren.item(rightIndex));

						if (comparison.getLeftDifference() == null) {
							System.out.println(indent + "-- MATCHED [" + leftFirstUnmatched + ", " + rightIndex + "] - " + writeXPath(leftChildren.item(leftFirstUnmatched)));
							rightFirstUnmatched = rightIndex + 1;
							continue nextLeftUnmatched;
						}
						else {
							if (leftFirstComparison == null)
								leftFirstComparison = comparison;
						}
					}
				}

				if (comparison == null) {
					leftFirstComparison = new NodeComparison(doc);
					leftFirstComparison.compare(leftChildren.item(leftFirstUnmatched), null);
				}

				if (leftFirstComparison != null) {
					leftDifference = buildDifference(leftDifference, leftNode, leftFirstComparison.getLeftDifference());
					System.out.println(indent + "-- LEFT DIFFERENCE - \n" + writeNode(leftDifference));
				}
			}

			nextRightUnmatched: for (int leftFirstUnmatched = 0, rightFirstUnmatched = 0; rightFirstUnmatched < rightLength; rightFirstUnmatched++) {
				NodeComparison rightFirstComparison = null, comparison = null;

				if (rightChildren.item(rightFirstUnmatched).getNodeType() == Node.TEXT_NODE)
					continue;

				for (int leftIndex = leftFirstUnmatched; leftIndex < leftLength; leftIndex++) {
					if (leftChildren.item(leftIndex).getNodeType() != Node.TEXT_NODE) {
						comparison = new NodeComparison(doc);

						System.out.println(indent+ "compare ["+ leftIndex+ ", "+ rightFirstUnmatched+ "] - "+ writeXPath(leftChildren.item(leftIndex))+ " - "+ writeXPath(rightChildren.item(rightFirstUnmatched)));
						comparison.compare(leftChildren.item(leftIndex),rightChildren.item(rightFirstUnmatched));

						if (comparison.getRightDifference() == null) {
							System.out.println(indent+ "-- MATCHED ["+ leftIndex+ ", "+ rightFirstUnmatched+ "] - "+ writeXPath(leftChildren.item(leftIndex)));
							leftFirstUnmatched = leftIndex + 1;
							continue nextRightUnmatched;
						}
						else {
							if (rightFirstComparison == null)
								rightFirstComparison = comparison;
						}
					}
				}

				if (comparison == null) {
					rightFirstComparison = new NodeComparison(doc);
					rightFirstComparison.compare(null, rightChildren.item(rightFirstUnmatched));
				}

				if (rightFirstComparison != null) {
					rightDifference = buildDifference(rightDifference,rightNode, rightFirstComparison.getRightDifference());
					System.out.println(indent+ "-- RIGHT DIFFERENCE - \n"+ writeNode(rightDifference));
				}
			}
		}

		private Node buildDifference(Node target, Node source, Node difference) {
			if (target == null) {
				target = doc.importNode(source, false);
			}

			target.appendChild(doc.importNode(difference, true));

			return target;
		}

		public Node getLeftDifference() {
			return leftDifference;
		}

		public Node getRightDifference() {
			return rightDifference;
		}

	}

	public static NodeComparison compareNodes(Node leftNode, Node rightNode) {
		NodeComparison comparison = new NodeComparison();
		comparison.compare(leftNode, rightNode);
		return comparison;
	}

	public static String writeXPath(Node node) {
		String path = "";
		while (node != null) {
			path = "/" + node.getNodeName() + path;
			node = node.getParentNode();
		}

		return path;
	}

	public static String writeNode(Node node) {
		if (node == null)
			return "null";

		try {
			OutputFormat f = new OutputFormat();
			f.setIndenting(true);
			f.setOmitXMLDeclaration(true);

			StringWriter w = new StringWriter();

			XMLSerializer output = new XMLSerializer(w, f);
			output.serialize((Element)node);

			return w.toString();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
}