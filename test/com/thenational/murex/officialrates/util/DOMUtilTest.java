package com.thenational.murex.officialrates.util;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;

import com.thenational.murex.officialrates.util.DOMUtil.NodeComparison;

public class DOMUtilTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testLoadDocument() {
		DOMUtil.loadDocument("test/input/test1.xml");
	}

	@Test
	public final void testCompareNodes() {
		Document left = DOMUtil.loadDocument("test/input/test1.xml");
		Document right = DOMUtil.loadDocument("test/input/test2.xml");

		NodeComparison comparison = DOMUtil.compareNodes(left, right);

		System.out.println(DOMUtil.writeNode(comparison.getLeftDifference()));
		System.out.println(DOMUtil.writeNode(comparison.getRightDifference()));
	}

}
